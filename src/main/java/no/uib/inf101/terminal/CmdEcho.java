package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < args.length; i++) {
            sb.append(args[i] + " ");
        }
        String str = sb.toString();

        return str;
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
        return "Printer det samme som du skrev inn";
    }

}
